package ru.androiddevschool.danio;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.HashMap;

import ru.androiddevschool.danio.Screens.Menu;

public class DanIO  extends Game {
	public static DanIO instance = new DanIO();
	private DanIO(){}
	public static DanIO get(){return instance;}

	@Override
	public void create () {
		batch = new SpriteBatch();
		screens = new HashMap<String, Screen>();
		screens.put("Menu", new Menu(batch));

		setScreen("Menu");
	}

	public void setScreen(String name){
		if (screens.containsKey(name))
			setScreen(screens.get(name));
	}
	private SpriteBatch batch;
	private HashMap<String, Screen> screens;

}