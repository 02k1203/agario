package ru.androiddevschool.danio.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import ru.androiddevschool.danio.DanIO;

/**
 * Created by 02k1203 on 06.04.2017.
 */


public class ScreenTraveler extends ClickListener {
    private String name;

    public ScreenTraveler(String name) {
        super();
        this.name = name;
    }

    public void clicked(InputEvent event, float x, float y) {
        DanIO.get().setScreen(name);
    }
}

