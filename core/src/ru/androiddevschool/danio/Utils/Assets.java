package ru.androiddevschool.danio.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.HashMap;

/**
 * Created by ga_nesterchuk on 06.04.2017.
 */
public class Assets {
    private static Assets ourInstance = new Assets();

    public static Assets get() {
        return ourInstance;
    }

    private Assets() {
        initImages();
        initFonts();
        initStyles();
        //for(String f : imgs.keySet()) System.out.println(f);
    }

    private void initStyles() {
        btnStyles = new HashMap<String, Button.ButtonStyle>();
    }

    private void initFonts() {
        fonts = new HashMap<String, BitmapFont>();
    }

    private void initImages() {
        imgs = new HashMap<String, TextureRegionDrawable>();
        //System.out.println("init");
        addFolderImg(getHandle("ui/"), "");
        //addFolderImg(getHandle(""), "");
    }

    private void addFolderImg(FileHandle file, String prefix) {
        //System.out.println(file.path());
        if (file.isDirectory())
            for (FileHandle f : file.list())
                addFolderImg(f, prefix + file.name());
        else if (file.extension().equals("png") || file.extension().equals("jpg"))
            imgs.put(prefix + "/" + file.nameWithoutExtension(), makeDrawable(file));
    }

    private TextureRegionDrawable makeDrawable(FileHandle handle) {
        return new TextureRegionDrawable(new TextureRegion(new Texture(handle)));
    }

    private FileHandle getHandle(String fileName) {
        return Gdx.files.internal(fileName);
    }

    public HashMap<String, TextureRegionDrawable> imgs;
    public HashMap<String, Button.ButtonStyle> btnStyles;
    public HashMap<String, BitmapFont> fonts;
}