package ru.androiddevschool.danio.Utils;

import com.badlogic.gdx.Gdx;

/**
 * Created by 02k1203 on 06.04.2017.
 */
public class Values {
    public static final float WORLD_WIDTH = 1920;
    public static final float WORLD_HEIGHT = 1080;
    public static final float ppuX = Gdx.graphics.getWidth()/WORLD_WIDTH;
    public static final float ppuY = Gdx.graphics.getWidth()/WORLD_HEIGHT;
}