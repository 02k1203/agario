package ru.androiddevschool.danio.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import ru.androiddevschool.danio.DanIO;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.height=540;
		config.width=960;
		new LwjglApplication(DanIO.get(), config);
	}
}
